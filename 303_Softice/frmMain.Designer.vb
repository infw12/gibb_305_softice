﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblCoin1 = New System.Windows.Forms.Label()
        Me.lblCoin2 = New System.Windows.Forms.Label()
        Me.lblCoin4 = New System.Windows.Forms.Label()
        Me.lblCoin3 = New System.Windows.Forms.Label()
        Me.lblCoin5 = New System.Windows.Forms.Label()
        Me.lblCoin6 = New System.Windows.Forms.Label()
        Me.lblCoin7 = New System.Windows.Forms.Label()
        Me.cmdConfirm = New System.Windows.Forms.Button()
        Me.lblCostTotal = New System.Windows.Forms.Label()
        Me.lstSofticeFlavours = New System.Windows.Forms.ListBox()
        Me.lstSofticeScoops = New System.Windows.Forms.ListBox()
        Me.lblCostPaid = New System.Windows.Forms.Label()
        Me.lblCostRemaining = New System.Windows.Forms.Label()
        Me.txtCurrentSofticeSize = New System.Windows.Forms.TextBox()
        Me.lstSofticeSizes = New System.Windows.Forms.ListBox()
        Me.txtCurrentSofticeFlavourPrice = New System.Windows.Forms.TextBox()
        Me.lstSofticeContainers = New System.Windows.Forms.ListBox()
        Me.txtCurrentSofticeContainer = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.pctCurrentSofticeFlavour = New System.Windows.Forms.PictureBox()
        Me.cmdSizeLeft = New System.Windows.Forms.Button()
        Me.pctCurrentSofticeContainer = New System.Windows.Forms.PictureBox()
        Me.cmdSizeRight = New System.Windows.Forms.Button()
        Me.cmdFlavourLeft = New System.Windows.Forms.Button()
        Me.cmdContainerLeft = New System.Windows.Forms.Button()
        Me.cmdFlavourRight = New System.Windows.Forms.Button()
        Me.cmdContainerRight = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmdCoin3 = New System.Windows.Forms.Button()
        Me.cmdCoin4 = New System.Windows.Forms.Button()
        Me.cmdCoin2 = New System.Windows.Forms.Button()
        Me.cmdCoin5 = New System.Windows.Forms.Button()
        Me.cmdCoin6 = New System.Windows.Forms.Button()
        Me.cmdCoin7 = New System.Windows.Forms.Button()
        Me.cmdCoin1 = New System.Windows.Forms.Button()
        Me.lblCostContainer = New System.Windows.Forms.Label()
        Me.lblCostScoops = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.tmrSofticeAnimation = New System.Windows.Forms.Timer(Me.components)
        Me.pctSofticeOutput = New System.Windows.Forms.PictureBox()
        Me.chkUseAnimation = New System.Windows.Forms.CheckBox()
        Me.Panel1.SuspendLayout()
        CType(Me.pctCurrentSofticeFlavour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctCurrentSofticeContainer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.pctSofticeOutput, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCoin1
        '
        Me.lblCoin1.Location = New System.Drawing.Point(10, 10)
        Me.lblCoin1.Name = "lblCoin1"
        Me.lblCoin1.Size = New System.Drawing.Size(75, 75)
        Me.lblCoin1.TabIndex = 1
        Me.lblCoin1.Text = "c1"
        Me.lblCoin1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCoin2
        '
        Me.lblCoin2.Location = New System.Drawing.Point(10, 90)
        Me.lblCoin2.Name = "lblCoin2"
        Me.lblCoin2.Size = New System.Drawing.Size(75, 75)
        Me.lblCoin2.TabIndex = 1
        Me.lblCoin2.Text = "c2"
        Me.lblCoin2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCoin4
        '
        Me.lblCoin4.Location = New System.Drawing.Point(10, 330)
        Me.lblCoin4.Name = "lblCoin4"
        Me.lblCoin4.Size = New System.Drawing.Size(75, 75)
        Me.lblCoin4.TabIndex = 1
        Me.lblCoin4.Text = "c4"
        Me.lblCoin4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCoin3
        '
        Me.lblCoin3.Location = New System.Drawing.Point(10, 170)
        Me.lblCoin3.Name = "lblCoin3"
        Me.lblCoin3.Size = New System.Drawing.Size(75, 75)
        Me.lblCoin3.TabIndex = 1
        Me.lblCoin3.Text = "c3"
        Me.lblCoin3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCoin5
        '
        Me.lblCoin5.Location = New System.Drawing.Point(10, 250)
        Me.lblCoin5.Name = "lblCoin5"
        Me.lblCoin5.Size = New System.Drawing.Size(75, 75)
        Me.lblCoin5.TabIndex = 1
        Me.lblCoin5.Text = "c5"
        Me.lblCoin5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCoin6
        '
        Me.lblCoin6.Location = New System.Drawing.Point(10, 410)
        Me.lblCoin6.Name = "lblCoin6"
        Me.lblCoin6.Size = New System.Drawing.Size(75, 75)
        Me.lblCoin6.TabIndex = 1
        Me.lblCoin6.Text = "c6"
        Me.lblCoin6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCoin7
        '
        Me.lblCoin7.Location = New System.Drawing.Point(10, 490)
        Me.lblCoin7.Name = "lblCoin7"
        Me.lblCoin7.Size = New System.Drawing.Size(75, 75)
        Me.lblCoin7.TabIndex = 1
        Me.lblCoin7.Text = "c7"
        Me.lblCoin7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdConfirm
        '
        Me.cmdConfirm.Location = New System.Drawing.Point(460, 370)
        Me.cmdConfirm.Name = "cmdConfirm"
        Me.cmdConfirm.Size = New System.Drawing.Size(120, 80)
        Me.cmdConfirm.TabIndex = 3
        Me.cmdConfirm.Text = "Confirm"
        Me.cmdConfirm.UseVisualStyleBackColor = True
        '
        'lblCostTotal
        '
        Me.lblCostTotal.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblCostTotal.AutoSize = True
        Me.lblCostTotal.Location = New System.Drawing.Point(4, 54)
        Me.lblCostTotal.Name = "lblCostTotal"
        Me.lblCostTotal.Size = New System.Drawing.Size(39, 13)
        Me.lblCostTotal.TabIndex = 4
        Me.lblCostTotal.Text = "Label1"
        '
        'lstSofticeFlavours
        '
        Me.lstSofticeFlavours.FormattingEnabled = True
        Me.lstSofticeFlavours.Location = New System.Drawing.Point(280, 10)
        Me.lstSofticeFlavours.Name = "lstSofticeFlavours"
        Me.lstSofticeFlavours.Size = New System.Drawing.Size(30, 95)
        Me.lstSofticeFlavours.TabIndex = 5
        Me.lstSofticeFlavours.Visible = False
        '
        'lstSofticeScoops
        '
        Me.lstSofticeScoops.FormattingEnabled = True
        Me.lstSofticeScoops.Location = New System.Drawing.Point(280, 320)
        Me.lstSofticeScoops.Name = "lstSofticeScoops"
        Me.lstSofticeScoops.Size = New System.Drawing.Size(30, 251)
        Me.lstSofticeScoops.TabIndex = 6
        Me.lstSofticeScoops.Visible = False
        '
        'lblCostPaid
        '
        Me.lblCostPaid.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblCostPaid.AutoSize = True
        Me.lblCostPaid.Location = New System.Drawing.Point(4, 78)
        Me.lblCostPaid.Name = "lblCostPaid"
        Me.lblCostPaid.Size = New System.Drawing.Size(39, 13)
        Me.lblCostPaid.TabIndex = 4
        Me.lblCostPaid.Text = "Label1"
        '
        'lblCostRemaining
        '
        Me.lblCostRemaining.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblCostRemaining.AutoSize = True
        Me.lblCostRemaining.Location = New System.Drawing.Point(4, 104)
        Me.lblCostRemaining.Name = "lblCostRemaining"
        Me.lblCostRemaining.Size = New System.Drawing.Size(39, 13)
        Me.lblCostRemaining.TabIndex = 4
        Me.lblCostRemaining.Text = "Label1"
        '
        'txtCurrentSofticeSize
        '
        Me.txtCurrentSofticeSize.Location = New System.Drawing.Point(70, 160)
        Me.txtCurrentSofticeSize.Multiline = True
        Me.txtCurrentSofticeSize.Name = "txtCurrentSofticeSize"
        Me.txtCurrentSofticeSize.ReadOnly = True
        Me.txtCurrentSofticeSize.Size = New System.Drawing.Size(110, 25)
        Me.txtCurrentSofticeSize.TabIndex = 7
        Me.txtCurrentSofticeSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lstSofticeSizes
        '
        Me.lstSofticeSizes.FormattingEnabled = True
        Me.lstSofticeSizes.Location = New System.Drawing.Point(280, 110)
        Me.lstSofticeSizes.Name = "lstSofticeSizes"
        Me.lstSofticeSizes.Size = New System.Drawing.Size(30, 108)
        Me.lstSofticeSizes.TabIndex = 6
        Me.lstSofticeSizes.Visible = False
        '
        'txtCurrentSofticeFlavourPrice
        '
        Me.txtCurrentSofticeFlavourPrice.Location = New System.Drawing.Point(70, 130)
        Me.txtCurrentSofticeFlavourPrice.Multiline = True
        Me.txtCurrentSofticeFlavourPrice.Name = "txtCurrentSofticeFlavourPrice"
        Me.txtCurrentSofticeFlavourPrice.ReadOnly = True
        Me.txtCurrentSofticeFlavourPrice.Size = New System.Drawing.Size(110, 25)
        Me.txtCurrentSofticeFlavourPrice.TabIndex = 7
        Me.txtCurrentSofticeFlavourPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lstSofticeContainers
        '
        Me.lstSofticeContainers.FormattingEnabled = True
        Me.lstSofticeContainers.Location = New System.Drawing.Point(280, 220)
        Me.lstSofticeContainers.Name = "lstSofticeContainers"
        Me.lstSofticeContainers.Size = New System.Drawing.Size(30, 95)
        Me.lstSofticeContainers.TabIndex = 9
        Me.lstSofticeContainers.Visible = False
        '
        'txtCurrentSofticeContainer
        '
        Me.txtCurrentSofticeContainer.Location = New System.Drawing.Point(70, 310)
        Me.txtCurrentSofticeContainer.Multiline = True
        Me.txtCurrentSofticeContainer.Name = "txtCurrentSofticeContainer"
        Me.txtCurrentSofticeContainer.ReadOnly = True
        Me.txtCurrentSofticeContainer.Size = New System.Drawing.Size(110, 25)
        Me.txtCurrentSofticeContainer.TabIndex = 7
        Me.txtCurrentSofticeContainer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.pctCurrentSofticeFlavour)
        Me.Panel1.Controls.Add(Me.cmdSizeLeft)
        Me.Panel1.Controls.Add(Me.pctCurrentSofticeContainer)
        Me.Panel1.Controls.Add(Me.cmdSizeRight)
        Me.Panel1.Controls.Add(Me.cmdFlavourLeft)
        Me.Panel1.Controls.Add(Me.txtCurrentSofticeFlavourPrice)
        Me.Panel1.Controls.Add(Me.cmdContainerLeft)
        Me.Panel1.Controls.Add(Me.txtCurrentSofticeContainer)
        Me.Panel1.Controls.Add(Me.cmdFlavourRight)
        Me.Panel1.Controls.Add(Me.txtCurrentSofticeSize)
        Me.Panel1.Controls.Add(Me.cmdContainerRight)
        Me.Panel1.Location = New System.Drawing.Point(320, 10)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(250, 350)
        Me.Panel1.TabIndex = 10
        '
        'pctCurrentSofticeFlavour
        '
        Me.pctCurrentSofticeFlavour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pctCurrentSofticeFlavour.Location = New System.Drawing.Point(70, 10)
        Me.pctCurrentSofticeFlavour.Name = "pctCurrentSofticeFlavour"
        Me.pctCurrentSofticeFlavour.Size = New System.Drawing.Size(110, 110)
        Me.pctCurrentSofticeFlavour.TabIndex = 8
        Me.pctCurrentSofticeFlavour.TabStop = False
        '
        'cmdSizeLeft
        '
        Me.cmdSizeLeft.BackgroundImage = CType(resources.GetObject("cmdSizeLeft.BackgroundImage"), System.Drawing.Image)
        Me.cmdSizeLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdSizeLeft.Location = New System.Drawing.Point(10, 160)
        Me.cmdSizeLeft.Name = "cmdSizeLeft"
        Me.cmdSizeLeft.Size = New System.Drawing.Size(50, 25)
        Me.cmdSizeLeft.TabIndex = 3
        Me.cmdSizeLeft.UseVisualStyleBackColor = True
        '
        'pctCurrentSofticeContainer
        '
        Me.pctCurrentSofticeContainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pctCurrentSofticeContainer.Location = New System.Drawing.Point(70, 190)
        Me.pctCurrentSofticeContainer.Name = "pctCurrentSofticeContainer"
        Me.pctCurrentSofticeContainer.Size = New System.Drawing.Size(110, 110)
        Me.pctCurrentSofticeContainer.TabIndex = 8
        Me.pctCurrentSofticeContainer.TabStop = False
        '
        'cmdSizeRight
        '
        Me.cmdSizeRight.BackgroundImage = Global._303_Softice.My.Resources.Resources.arrow_right
        Me.cmdSizeRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdSizeRight.Location = New System.Drawing.Point(190, 160)
        Me.cmdSizeRight.Name = "cmdSizeRight"
        Me.cmdSizeRight.Size = New System.Drawing.Size(50, 25)
        Me.cmdSizeRight.TabIndex = 3
        Me.cmdSizeRight.UseVisualStyleBackColor = True
        '
        'cmdFlavourLeft
        '
        Me.cmdFlavourLeft.BackgroundImage = CType(resources.GetObject("cmdFlavourLeft.BackgroundImage"), System.Drawing.Image)
        Me.cmdFlavourLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdFlavourLeft.Location = New System.Drawing.Point(10, 60)
        Me.cmdFlavourLeft.Name = "cmdFlavourLeft"
        Me.cmdFlavourLeft.Size = New System.Drawing.Size(50, 25)
        Me.cmdFlavourLeft.TabIndex = 3
        Me.cmdFlavourLeft.UseVisualStyleBackColor = True
        '
        'cmdContainerLeft
        '
        Me.cmdContainerLeft.BackgroundImage = CType(resources.GetObject("cmdContainerLeft.BackgroundImage"), System.Drawing.Image)
        Me.cmdContainerLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdContainerLeft.Location = New System.Drawing.Point(10, 310)
        Me.cmdContainerLeft.Name = "cmdContainerLeft"
        Me.cmdContainerLeft.Size = New System.Drawing.Size(50, 25)
        Me.cmdContainerLeft.TabIndex = 3
        Me.cmdContainerLeft.UseVisualStyleBackColor = True
        '
        'cmdFlavourRight
        '
        Me.cmdFlavourRight.BackgroundImage = Global._303_Softice.My.Resources.Resources.arrow_right
        Me.cmdFlavourRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdFlavourRight.Location = New System.Drawing.Point(190, 60)
        Me.cmdFlavourRight.Name = "cmdFlavourRight"
        Me.cmdFlavourRight.Size = New System.Drawing.Size(50, 25)
        Me.cmdFlavourRight.TabIndex = 3
        Me.cmdFlavourRight.UseVisualStyleBackColor = True
        '
        'cmdContainerRight
        '
        Me.cmdContainerRight.BackgroundImage = Global._303_Softice.My.Resources.Resources.arrow_right
        Me.cmdContainerRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdContainerRight.Location = New System.Drawing.Point(190, 310)
        Me.cmdContainerRight.Name = "cmdContainerRight"
        Me.cmdContainerRight.Size = New System.Drawing.Size(50, 25)
        Me.cmdContainerRight.TabIndex = 3
        Me.cmdContainerRight.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblCoin1)
        Me.Panel2.Controls.Add(Me.cmdCoin3)
        Me.Panel2.Controls.Add(Me.cmdCoin4)
        Me.Panel2.Controls.Add(Me.cmdCoin2)
        Me.Panel2.Controls.Add(Me.lblCoin7)
        Me.Panel2.Controls.Add(Me.cmdCoin5)
        Me.Panel2.Controls.Add(Me.lblCoin6)
        Me.Panel2.Controls.Add(Me.cmdCoin6)
        Me.Panel2.Controls.Add(Me.cmdCoin7)
        Me.Panel2.Controls.Add(Me.lblCoin2)
        Me.Panel2.Controls.Add(Me.lblCoin5)
        Me.Panel2.Controls.Add(Me.lblCoin4)
        Me.Panel2.Controls.Add(Me.cmdCoin1)
        Me.Panel2.Controls.Add(Me.lblCoin3)
        Me.Panel2.Location = New System.Drawing.Point(590, 10)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(190, 580)
        Me.Panel2.TabIndex = 11
        '
        'cmdCoin3
        '
        Me.cmdCoin3.BackgroundImage = Global._303_Softice.My.Resources.Resources.money_100
        Me.cmdCoin3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdCoin3.Location = New System.Drawing.Point(100, 170)
        Me.cmdCoin3.Name = "cmdCoin3"
        Me.cmdCoin3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCoin3.Size = New System.Drawing.Size(75, 75)
        Me.cmdCoin3.TabIndex = 0
        Me.cmdCoin3.Tag = "100"
        Me.cmdCoin3.UseVisualStyleBackColor = True
        '
        'cmdCoin4
        '
        Me.cmdCoin4.BackgroundImage = Global._303_Softice.My.Resources.Resources.money_50
        Me.cmdCoin4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdCoin4.Location = New System.Drawing.Point(100, 250)
        Me.cmdCoin4.Name = "cmdCoin4"
        Me.cmdCoin4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCoin4.Size = New System.Drawing.Size(75, 75)
        Me.cmdCoin4.TabIndex = 0
        Me.cmdCoin4.Tag = "50"
        Me.cmdCoin4.UseVisualStyleBackColor = True
        '
        'cmdCoin2
        '
        Me.cmdCoin2.BackgroundImage = Global._303_Softice.My.Resources.Resources.money_200
        Me.cmdCoin2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdCoin2.Location = New System.Drawing.Point(100, 90)
        Me.cmdCoin2.Name = "cmdCoin2"
        Me.cmdCoin2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCoin2.Size = New System.Drawing.Size(75, 75)
        Me.cmdCoin2.TabIndex = 0
        Me.cmdCoin2.Tag = "200"
        Me.cmdCoin2.UseVisualStyleBackColor = True
        '
        'cmdCoin5
        '
        Me.cmdCoin5.BackgroundImage = Global._303_Softice.My.Resources.Resources.money_20
        Me.cmdCoin5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdCoin5.Location = New System.Drawing.Point(100, 330)
        Me.cmdCoin5.Name = "cmdCoin5"
        Me.cmdCoin5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCoin5.Size = New System.Drawing.Size(75, 75)
        Me.cmdCoin5.TabIndex = 0
        Me.cmdCoin5.Tag = "20"
        Me.cmdCoin5.UseVisualStyleBackColor = True
        '
        'cmdCoin6
        '
        Me.cmdCoin6.BackgroundImage = Global._303_Softice.My.Resources.Resources.money_10
        Me.cmdCoin6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdCoin6.Location = New System.Drawing.Point(100, 410)
        Me.cmdCoin6.Name = "cmdCoin6"
        Me.cmdCoin6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCoin6.Size = New System.Drawing.Size(75, 75)
        Me.cmdCoin6.TabIndex = 0
        Me.cmdCoin6.Tag = "10"
        Me.cmdCoin6.UseVisualStyleBackColor = True
        '
        'cmdCoin7
        '
        Me.cmdCoin7.BackgroundImage = Global._303_Softice.My.Resources.Resources.money_5
        Me.cmdCoin7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdCoin7.Location = New System.Drawing.Point(100, 490)
        Me.cmdCoin7.Name = "cmdCoin7"
        Me.cmdCoin7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCoin7.Size = New System.Drawing.Size(75, 75)
        Me.cmdCoin7.TabIndex = 0
        Me.cmdCoin7.Tag = "5"
        Me.cmdCoin7.UseVisualStyleBackColor = True
        '
        'cmdCoin1
        '
        Me.cmdCoin1.BackgroundImage = CType(resources.GetObject("cmdCoin1.BackgroundImage"), System.Drawing.Image)
        Me.cmdCoin1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.cmdCoin1.Location = New System.Drawing.Point(100, 10)
        Me.cmdCoin1.Name = "cmdCoin1"
        Me.cmdCoin1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCoin1.Size = New System.Drawing.Size(75, 75)
        Me.cmdCoin1.TabIndex = 0
        Me.cmdCoin1.Tag = "500"
        Me.cmdCoin1.UseVisualStyleBackColor = True
        '
        'lblCostContainer
        '
        Me.lblCostContainer.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblCostContainer.AutoSize = True
        Me.lblCostContainer.Location = New System.Drawing.Point(4, 30)
        Me.lblCostContainer.Name = "lblCostContainer"
        Me.lblCostContainer.Size = New System.Drawing.Size(39, 13)
        Me.lblCostContainer.TabIndex = 4
        Me.lblCostContainer.Text = "Label1"
        '
        'lblCostScoops
        '
        Me.lblCostScoops.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblCostScoops.AutoSize = True
        Me.lblCostScoops.Location = New System.Drawing.Point(4, 6)
        Me.lblCostScoops.Name = "lblCostScoops"
        Me.lblCostScoops.Size = New System.Drawing.Size(39, 13)
        Me.lblCostScoops.TabIndex = 4
        Me.lblCostScoops.Text = "Label1"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lblCostScoops, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblCostContainer, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblCostPaid, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.lblCostRemaining, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.lblCostTotal, 0, 2)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(320, 460)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.Padding = New System.Windows.Forms.Padding(0, 0, 0, 5)
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(260, 130)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(320, 370)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(120, 80)
        Me.cmdCancel.TabIndex = 12
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(100, 10)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "Tilt"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tmrSofticeAnimation
        '
        Me.tmrSofticeAnimation.Interval = 500
        '
        'pctSofticeOutput
        '
        Me.pctSofticeOutput.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pctSofticeOutput.Location = New System.Drawing.Point(10, 260)
        Me.pctSofticeOutput.Name = "pctSofticeOutput"
        Me.pctSofticeOutput.Size = New System.Drawing.Size(270, 325)
        Me.pctSofticeOutput.TabIndex = 13
        Me.pctSofticeOutput.TabStop = False
        '
        'chkUseAnimation
        '
        Me.chkUseAnimation.AutoSize = True
        Me.chkUseAnimation.Location = New System.Drawing.Point(10, 10)
        Me.chkUseAnimation.Name = "chkUseAnimation"
        Me.chkUseAnimation.Size = New System.Drawing.Size(70, 17)
        Me.chkUseAnimation.TabIndex = 15
        Me.chkUseAnimation.Text = "Animated"
        Me.chkUseAnimation.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(793, 595)
        Me.Controls.Add(Me.chkUseAnimation)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.pctSofticeOutput)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lstSofticeContainers)
        Me.Controls.Add(Me.lstSofticeScoops)
        Me.Controls.Add(Me.lstSofticeSizes)
        Me.Controls.Add(Me.lstSofticeFlavours)
        Me.Controls.Add(Me.cmdConfirm)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmMain"
        Me.Text = " "
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.pctCurrentSofticeFlavour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctCurrentSofticeContainer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.pctSofticeOutput, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdCoin1 As System.Windows.Forms.Button
    Friend WithEvents lblCoin1 As System.Windows.Forms.Label
    Friend WithEvents cmdCoin2 As System.Windows.Forms.Button
    Friend WithEvents lblCoin2 As System.Windows.Forms.Label
    Friend WithEvents cmdCoin3 As System.Windows.Forms.Button
    Friend WithEvents cmdCoin4 As System.Windows.Forms.Button
    Friend WithEvents lblCoin4 As System.Windows.Forms.Label
    Friend WithEvents lblCoin3 As System.Windows.Forms.Label
    Friend WithEvents cmdCoin5 As System.Windows.Forms.Button
    Friend WithEvents lblCoin5 As System.Windows.Forms.Label
    Friend WithEvents lblCoin6 As System.Windows.Forms.Label
    Friend WithEvents lblCoin7 As System.Windows.Forms.Label
    Friend WithEvents cmdCoin6 As System.Windows.Forms.Button
    Friend WithEvents cmdCoin7 As System.Windows.Forms.Button
    Friend WithEvents cmdConfirm As System.Windows.Forms.Button
    Friend WithEvents lblCostTotal As System.Windows.Forms.Label
    Friend WithEvents lstSofticeFlavours As System.Windows.Forms.ListBox
    Friend WithEvents lstSofticeScoops As System.Windows.Forms.ListBox
    Friend WithEvents lblCostPaid As System.Windows.Forms.Label
    Friend WithEvents lblCostRemaining As System.Windows.Forms.Label
    Friend WithEvents cmdSizeLeft As System.Windows.Forms.Button
    Friend WithEvents cmdSizeRight As System.Windows.Forms.Button
    Friend WithEvents txtCurrentSofticeSize As System.Windows.Forms.TextBox
    Friend WithEvents lstSofticeSizes As System.Windows.Forms.ListBox
    Friend WithEvents pctCurrentSofticeFlavour As System.Windows.Forms.PictureBox
    Friend WithEvents cmdFlavourLeft As System.Windows.Forms.Button
    Friend WithEvents cmdFlavourRight As System.Windows.Forms.Button
    Friend WithEvents txtCurrentSofticeFlavourPrice As System.Windows.Forms.TextBox
    Friend WithEvents lstSofticeContainers As System.Windows.Forms.ListBox
    Friend WithEvents cmdContainerLeft As System.Windows.Forms.Button
    Friend WithEvents cmdContainerRight As System.Windows.Forms.Button
    Friend WithEvents txtCurrentSofticeContainer As System.Windows.Forms.TextBox
    Friend WithEvents pctCurrentSofticeContainer As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCostScoops As System.Windows.Forms.Label
    Friend WithEvents lblCostContainer As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents pctSofticeOutput As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tmrSofticeAnimation As System.Windows.Forms.Timer
    Friend WithEvents chkUseAnimation As System.Windows.Forms.CheckBox

End Class
