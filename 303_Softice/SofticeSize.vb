﻿Public Class SofticeSize

    Friend Name As String
    Friend PriceMultiplier As Single

    Friend Sub New(ByVal Name As String, ByVal PriceMultiplier As Single)

        Me.Name = Name
        Me.PriceMultiplier = PriceMultiplier

    End Sub

    Friend Function PrettyPrint() As String
        Return String.Format("{0} (Price x {1})", Name, PriceMultiplier)
    End Function

End Class
