﻿Public Class SofticeScoop

    Friend Flavour As SofticeFlavour
    Friend Size As SofticeSize

    Friend Sub New(ByVal Flavour As SofticeFlavour, ByVal Size As SofticeSize)

        Me.Flavour = Flavour
        Me.Size = Size

    End Sub

    Friend Function CalculatePrice() As Single
        Return Me.Flavour.BasePrice * Me.Size.PriceMultiplier
    End Function

    Friend Function PrettyPrint() As String
        Return String.Format("{0} scoop of {1}, costing {2}", Size.Name, Flavour.Name, frmMain.FormatCurrency(CalculatePrice()))
    End Function

End Class
