﻿Public Class Softice

    Friend SofticeScoops As List(Of SofticeScoop)
    Friend SofticeContainer As SofticeContainer

    Friend Sub New()

        SofticeScoops = New List(Of SofticeScoop)
        SofticeContainer = New SofticeContainer("Cone", 100, My.Resources.container_cone, My.Resources.container_cone_front, My.Resources.container_cone_back)

    End Sub


    Friend Sub AddScoop(ByVal Scoop As SofticeScoop)
        SofticeScoops.Add(Scoop)
    End Sub

    Friend Sub RemoveScoop(ByVal Scoop As SofticeScoop)
        SofticeScoops.Remove(Scoop)
    End Sub

    Friend Sub ClearScoops()
        SofticeScoops.Clear()
    End Sub

    Friend Function PrettyPrint() As String

        Dim s As String = ""

        For Each scoop As SofticeScoop In SofticeScoops
            s += scoop.PrettyPrint & vbCrLf
        Next

        s += String.Format("In a: {0]", SofticeContainer.PrettyPrint())

        Return s

    End Function


    Friend Function CalculateScoopsPrice() As Integer

        Dim Price As Single = 0

        For Each Scoop As SofticeScoop In SofticeScoops
            Price += Scoop.CalculatePrice()
        Next

        ' Rounding to five cents, since that's the smallest coin.
        Return Math.Round(Price / 5) * 5

    End Function

    Friend Function CalculateContainerPrice() As Integer

        ' Rounding to five cents, since that's the smallest coin.
        Return Math.Round(SofticeContainer.CalculatePrice() / 5) * 5

    End Function

    Friend Function CalculateTotalPrice() As Integer

        Return CalculateScoopsPrice() + CalculateContainerPrice()

    End Function

   

End Class
