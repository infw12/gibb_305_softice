﻿Public Class SofticeContainer

    Friend Name As String
    Friend Price As Integer
    Friend Picture As Bitmap
    Friend PictureFront As Bitmap
    Friend PictureBack As Bitmap

    Friend Sub New(ByVal Name As String, ByVal Price As Integer, ByVal Picture As Bitmap, ByVal PictureFront As Bitmap, ByVal PictureBack As Bitmap)

        Me.Name = Name
        Me.Price = Price
        Me.Picture = Picture
        Me.PictureFront = PictureFront
        Me.PictureBack = PictureBack

    End Sub

    Friend Function PrettyPrint() As String
        Return String.Format("{0} ({1})", Name, frmMain.FormatCurrency(Price))
    End Function

    Friend Function CalculatePrice() As Integer
        Return Me.Price
    End Function

End Class
