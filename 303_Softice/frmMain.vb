﻿Public Class frmMain

    Private Const DEBUG As Boolean = False

    Private Softice As Softice

    Private Blocker As Integer = 100

    Private AvailableFlavoursList As List(Of SofticeFlavour)
    Private SelectedFlavourIndex As Integer

    Private AvailableSizesList As List(Of SofticeSize)
    Private SelectedSizeIndex As Integer

    Private AvailableContainersList As List(Of SofticeContainer)
    Private SelectedContainerIndex As Integer

    Private CostScoops As Integer
    Private CostContainer As Integer
    Private CostTotal As Integer
    Private CostPaid As Integer
    Private CostRemaining As Integer

    Private HasHadSoftServe As Boolean = False

    Private Sub frmFlavourManagement_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Softice = New Softice()
        AvailableFlavoursList = New List(Of SofticeFlavour)
        AvailableSizesList = New List(Of SofticeSize)
        AvailableContainersList = New List(Of SofticeContainer)

        SetUpProducts()
        SetUpInterface()

        UpdateInterface()

        InitiateSelectionMode()

        'MsgBox(String.Format("{0} loves {1}, said {2}, as a result of which {2} got hit by {1} though.", "John", "Anne", "Lars"))

    End Sub

    Private Sub SetUpProducts()

        AvailableFlavoursList.Clear()
        AvailableSizesList.Clear()

        AvailableFlavoursList.Add(New SofticeFlavour("Chocolate", 100, My.Resources.flavour_chocolate))
        AvailableFlavoursList.Add(New SofticeFlavour("Strawberry", 250, My.Resources.flavour_strawberry))
        AvailableFlavoursList.Add(New SofticeFlavour("Vanilla", 150, My.Resources.flavour_vanilla))

        AvailableSizesList.Add(New SofticeSize("Big", 1.5))
        AvailableSizesList.Add(New SofticeSize("Normal", 1))
        AvailableSizesList.Add(New SofticeSize("Small", 0.5))

        AvailableContainersList.Add(New SofticeContainer("Cup", 50, My.Resources.container_cup, My.Resources.container_cup_front, My.Resources.container_cup_back))
        AvailableContainersList.Add(New SofticeContainer("Cone", 100, My.Resources.container_cone, My.Resources.container_cone_front, My.Resources.container_cone_back))

    End Sub

    Private Sub SetUpInterface()

        lblCoin1.Text = FormatCurrency(cmdCoin1.Tag)
        lblCoin2.Text = FormatCurrency(cmdCoin2.Tag)
        lblCoin3.Text = FormatCurrency(cmdCoin3.Tag)
        lblCoin4.Text = FormatCurrency(cmdCoin4.Tag)
        lblCoin5.Text = FormatCurrency(cmdCoin5.Tag)
        lblCoin6.Text = FormatCurrency(cmdCoin6.Tag)
        lblCoin7.Text = FormatCurrency(cmdCoin7.Tag)

        pctCurrentSofticeContainer.BackgroundImage = Softice.SofticeContainer.Picture

        lstSofticeFlavours.Items.Clear()
        For Each Flavour As SofticeFlavour In AvailableFlavoursList
            lstSofticeFlavours.Items.Add(Flavour.PrettyPrint())
        Next

        lstSofticeSizes.Items.Clear()
        For Each Size As SofticeSize In AvailableSizesList
            lstSofticeSizes.Items.Add(Size.PrettyPrint())
        Next

        lstSofticeContainers.Items.Clear()
        For Each Container As SofticeContainer In AvailableContainersList
            lstSofticeContainers.Items.Add(Container.PrettyPrint())
        Next

        If lstSofticeSizes.Items.Count > 0 Then
            lstSofticeSizes.SelectedIndex = 0
        End If

        If lstSofticeFlavours.Items.Count > 0 Then
            lstSofticeFlavours.SelectedIndex = 0
        End If

        If lstSofticeContainers.Items.Count > 0 Then
            lstSofticeContainers.SelectedIndex = 0
            Softice.SofticeContainer = GetSelectedContainer()
        End If

    End Sub

    Private Sub UpdateInterface()

        lstSofticeScoops.Items.Clear()

        For Each Scoop As SofticeScoop In Me.Softice.SofticeScoops
            lstSofticeScoops.Items.Add(Scoop.PrettyPrint())
        Next

        lblCostContainer.Text = String.Format("Container: {0}", FormatCurrency(CostContainer))
        lblCostScoops.Text = String.Format("Scoops: {0}", FormatCurrency(CostScoops))
        lblCostTotal.Text = String.Format("Total: {0}", FormatCurrency(CostTotal))
        lblCostPaid.Text = String.Format("Paid: {0}", FormatCurrency(CostPaid))
        lblCostRemaining.Text = String.Format("Remaining: {0}", FormatCurrency(CostRemaining))

        lstSofticeSizes.SelectedIndex = SelectedSizeIndex
        lstSofticeFlavours.SelectedIndex = SelectedFlavourIndex
        lstSofticeContainers.SelectedIndex = SelectedContainerIndex

        txtCurrentSofticeSize.Text = GetSelectedSize().PrettyPrint()
        txtCurrentSofticeFlavourPrice.Text = GetSelectedFlavour().PrettyPrint()
        txtCurrentSofticeContainer.Text = GetSelectedContainer().PrettyPrint()

        pctCurrentSofticeFlavour.BackgroundImage = GetSelectedFlavour().Picture
        pctCurrentSofticeContainer.BackgroundImage = GetSelectedContainer().Picture


    End Sub

    Private Sub UpdateCosts()

        CostScoops = Softice.CalculateScoopsPrice()
        CostContainer = Softice.CalculateContainerPrice()
        CostTotal = Me.Softice.CalculateTotalPrice()
        CostRemaining = CostTotal - CostPaid
        'MsgBox(String.Format("Total: {0} --- Paid: {1} --- Remaining: {2}", CostTotal, CostPaid, CostRemaining))

    End Sub

    Private Sub ResetCosts()
        CostTotal = 0
        CostPaid = 0
        CostRemaining = 0
        CostScoops = 0
        CostContainer = 0
    End Sub

    Private Sub UpdateSoftice()

        Softice.SofticeContainer = GetSelectedContainer()
        Softice.ClearScoops()
        Softice.AddScoop(New SofticeScoop(GetSelectedFlavour(), GetSelectedSize()))

    End Sub


    Private Sub InitiateSelectionMode()

        cmdCoin1.Enabled = False
        cmdCoin2.Enabled = False
        cmdCoin3.Enabled = False
        cmdCoin4.Enabled = False
        cmdCoin5.Enabled = False
        cmdCoin6.Enabled = False
        cmdCoin7.Enabled = False

        cmdFlavourLeft.Enabled = True
        cmdFlavourRight.Enabled = True

        cmdSizeLeft.Enabled = True
        cmdSizeRight.Enabled = True

        cmdContainerLeft.Enabled = True
        cmdContainerRight.Enabled = True

        If DEBUG Then
            lstSofticeFlavours.Enabled = True
            lstSofticeSizes.Enabled = True
            lstSofticeContainers.Enabled = True

            lstSofticeFlavours.Visible = True
            lstSofticeSizes.Visible = True
            lstSofticeContainers.Visible = True
            lstSofticeScoops.Visible = True
        End If

        cmdConfirm.Enabled = True
        cmdCancel.Enabled = False

    End Sub

    Private Sub InitiatePaymentMode()

        cmdCoin1.Enabled = True
        cmdCoin2.Enabled = True
        cmdCoin3.Enabled = True
        cmdCoin4.Enabled = True
        cmdCoin5.Enabled = True
        cmdCoin6.Enabled = True
        cmdCoin7.Enabled = True

        cmdFlavourLeft.Enabled = False
        cmdFlavourRight.Enabled = False

        cmdSizeLeft.Enabled = False
        cmdSizeRight.Enabled = False

        cmdContainerLeft.Enabled = False
        cmdContainerRight.Enabled = False

        If DEBUG Then

            lstSofticeFlavours.Enabled = False
            lstSofticeSizes.Enabled = False
            lstSofticeContainers.Enabled = False

        End If

        cmdConfirm.Enabled = False
        cmdCancel.Enabled = True

    End Sub

    Private Sub InitiateOutputMode()

        cmdCoin1.Enabled = False
        cmdCoin2.Enabled = False
        cmdCoin3.Enabled = False
        cmdCoin4.Enabled = False
        cmdCoin5.Enabled = False
        cmdCoin6.Enabled = False
        cmdCoin7.Enabled = False

        cmdFlavourLeft.Enabled = False
        cmdFlavourRight.Enabled = False

        cmdSizeLeft.Enabled = False
        cmdSizeRight.Enabled = False

        cmdContainerLeft.Enabled = False
        cmdContainerRight.Enabled = False

        If DEBUG Then

            lstSofticeFlavours.Enabled = False
            lstSofticeSizes.Enabled = False
            lstSofticeContainers.Enabled = False

        End If

        cmdConfirm.Enabled = False
        cmdCancel.Enabled = False

    End Sub


    Private Function GetSelectedSize() As SofticeSize
        Return AvailableSizesList.Item(SelectedSizeIndex)
    End Function

    Private Sub ChangeSelectedSize(ByVal Increment As Boolean)

        If Increment Then
            Select Case SelectedSizeIndex
                Case AvailableSizesList.Count - 1
                    SelectedSizeIndex = 0
                Case Else
                    SelectedSizeIndex += 1
            End Select
        Else
            Select Case SelectedSizeIndex
                Case 0
                    SelectedSizeIndex = AvailableSizesList.Count - 1
                Case Else
                    SelectedSizeIndex -= 1
            End Select
        End If

        'MsgBox(String.Format("Selected index: {0}, Selected Size: {1}", SelectedSizeIndex, GetSelectedSize().Name))

    End Sub


    Private Function GetSelectedFlavour() As SofticeFlavour
        Return AvailableFlavoursList.Item(SelectedFlavourIndex)
    End Function

    Private Sub ChangeSelectedFlavour(ByVal Increment As Boolean)

        If Increment Then
            Select Case SelectedFlavourIndex
                Case AvailableFlavoursList.Count - 1
                    SelectedFlavourIndex = 0
                Case Else
                    SelectedFlavourIndex += 1
            End Select
        Else
            Select Case SelectedFlavourIndex
                Case 0
                    SelectedFlavourIndex = AvailableFlavoursList.Count - 1
                Case Else
                    SelectedFlavourIndex -= 1
            End Select
        End If

    End Sub


    Private Function GetSelectedContainer() As SofticeContainer
        Return AvailableContainersList.Item(SelectedContainerIndex)
    End Function

    Private Sub ChangeSelectedContainer(ByVal Increment As Boolean)

        If Increment Then
            Select Case SelectedContainerIndex
                Case AvailableContainersList.Count - 1
                    SelectedContainerIndex = 0
                Case Else
                    SelectedContainerIndex += 1
            End Select
        Else
            Select Case SelectedContainerIndex
                Case 0
                    SelectedContainerIndex = AvailableContainersList.Count - 1
                Case Else
                    SelectedContainerIndex -= 1
            End Select
        End If

    End Sub


    Friend Function FormatCurrency(ByVal Cents As Integer) As String
        Return String.Format("{0:C}", Cents / 100)
    End Function

    Private Sub cmdCoin_Click(sender As System.Object, e As System.EventArgs) Handles cmdCoin1.Click, cmdCoin2.Click, cmdCoin3.Click, cmdCoin4.Click, cmdCoin5.Click, cmdCoin6.Click, cmdCoin7.Click

        If CostRemaining - Convert.ToInt32(sender.tag) < 0 Then
            Return
        End If

        CostPaid += Convert.ToInt32(sender.tag)

        UpdateCosts()
        UpdateInterface()

        If CostRemaining = 0 Then
            InitiateOutputMode()
            StartSofticeOutput()
        End If
        'MsgBox(Convert.ToInt32(sender.tag))
        'MsgBox(CostPaid)
        'MsgBox(String.Format("{0:C}", CInt(sender.tag) / 100))

    End Sub


    Private Sub cmdConfirm_Click(sender As System.Object, e As System.EventArgs) Handles cmdConfirm.Click
        '' Updating the variables containing the total / paid price.
        'UpdateCosts()
        '' Updating the various labels with the total / paid / remaining price, as well as the list containing the selected scoops.
        'UpdateInterface()

        'If HasHadSoftServe Then
        '    MsgBox("No more soft serve for you today!")
        '    Return
        'End If

        InitiatePaymentMode()
        'Dim Scoop As New SofticeScoop(New SofticeFlavour("Chocolate", Convert.ToInt32(txta.Text)), New SofticeSize("Biiiig", Convert.ToInt32(txtb.Text)))
        'Softice.AddScoop(Scoop)
        'MsgBox(Softice.PrettyPrint)
        'lblHurp.Text = FormatCurrency(Softice.CalculatePrice.ToString)
    End Sub

    Private Sub cmdCancel_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancel.Click

        CostPaid = 0
        UpdateCosts()
        UpdateInterface()
        InitiateSelectionMode()

    End Sub


    Private Sub lstSofticeContainers_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lstSofticeContainers.SelectedIndexChanged
        SelectedContainerIndex = lstSofticeContainers.SelectedIndex
        UpdateSoftice()
        UpdateCosts()
        UpdateInterface()
    End Sub

    Private Sub cmdContainerLeft_Click(sender As System.Object, e As System.EventArgs) Handles cmdContainerLeft.Click
        ChangeSelectedContainer(False)
        UpdateSoftice()
        UpdateCosts()
        UpdateInterface()
    End Sub

    Private Sub cmdContainerRight_Click(sender As System.Object, e As System.EventArgs) Handles cmdContainerRight.Click
        ChangeSelectedContainer(True)
        UpdateSoftice()
        UpdateCosts()
        UpdateInterface()
    End Sub


    Private Sub lstSofticeSizes_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lstSofticeSizes.SelectedIndexChanged
        SelectedSizeIndex = lstSofticeSizes.SelectedIndex
        UpdateSoftice()
        UpdateCosts()
        UpdateInterface()
    End Sub

    Private Sub cmdSizeRight_Click(sender As System.Object, e As System.EventArgs) Handles cmdSizeRight.Click
        ChangeSelectedSize(True)
        UpdateSoftice()
        UpdateCosts()
        UpdateInterface()
    End Sub

    Private Sub cmdSizeLeft_Click(sender As System.Object, e As System.EventArgs) Handles cmdSizeLeft.Click
        ChangeSelectedSize(False)
        UpdateSoftice()
        UpdateCosts()
        UpdateInterface()
    End Sub


    Private Sub lstSofticeFlavours_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lstSofticeFlavours.SelectedIndexChanged
        SelectedFlavourIndex = lstSofticeFlavours.SelectedIndex
        UpdateSoftice()
        UpdateCosts()
        UpdateInterface()
    End Sub

    Private Sub cmdFlavourLeft_Click(sender As System.Object, e As System.EventArgs) Handles cmdFlavourLeft.Click
        ChangeSelectedFlavour(False)
        UpdateSoftice()
        UpdateCosts()
        UpdateInterface()
    End Sub

    Private Sub cmdFlavourRight_Click(sender As System.Object, e As System.EventArgs) Handles cmdFlavourRight.Click
        ChangeSelectedFlavour(True)
        UpdateSoftice()
        UpdateCosts()
        UpdateInterface()
    End Sub


    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click

        InitiateOutputMode()

        StartSofticeOutput()

    End Sub

    Private Sub StartSofticeOutput()
        'MsgBox("Is visible nao!")
        pctSofticeOutput.Visible = True

        If chkUseAnimation.Checked Then
            tmrSofticeAnimation.Start()
        Else
            AnimateSoftice(0)
        End If

    End Sub

    Private Sub AnimateSoftice(ByVal Blocker As Integer)

        Dim imgSoftice, imgCupBack, imgCupFront As Image

        Dim SizeMultiplier As Single = Softice.SofticeScoops.Item(0).Size.PriceMultiplier

        'i1 = Image.FromFile("C:\Users\Michael Senn\Desktop\303_Softice\303_Softice\resources\container_cup.png")
        'imgCup = Softice.SofticeContainer.Picture
        'imgCup = Softice.SofticeContainer.Picture
        imgCupFront = Softice.SofticeContainer.PictureFront
        imgCupBack = Softice.SofticeContainer.PictureBack
        'i2 = Image.FromFile("C:\Users\Michael Senn\Desktop\303_Softice\303_Softice\resources\flavour_chocolate.png")
        imgSoftice = Softice.SofticeScoops.Item(0).Flavour.Picture

        Dim WidthBox As Integer = pctSofticeOutput.Width
        Dim HeightBox As Integer = pctSofticeOutput.Height

        Dim g As Graphics
        g = pctSofticeOutput.CreateGraphics()

        g.Clear(Color.White)

        'Dim RectangleCup As Rectangle = ReturnFittingRectangle(pctSofticeOutput.Width, pctSofticeOutput.Height, imgCup)
        Dim RectangleCupFront As Rectangle = ReturnFittingRectangle(pctSofticeOutput.Width, pctSofticeOutput.Height, imgCupFront)
        Dim RectangleCupBack As Rectangle = ReturnFittingRectangle(pctSofticeOutput.Width, pctSofticeOutput.Height, imgCupBack)

        Dim RectangleSoftice As Rectangle = ReturnFittingRectangle(pctSofticeOutput.Width, pctSofticeOutput.Height, imgSoftice, SizeMultiplier)
        ' Shifting the rectange which'll contain the soft serve up a bit.
        RectangleSoftice.Y -= RectangleCupFront.Height - 20

        'g.DrawImage(imgCup, RectangleCup)
        'g.DrawImage(imgSoftice, RectangleSoftice)


        g.DrawImage(imgCupBack, RectangleCupBack)
        g.DrawImage(imgSoftice, RectangleSoftice)
        g.DrawImage(imgCupFront, RectangleCupFront)

        Dim BlockerRectangle As Rectangle
        BlockerRectangle.X = 0
        BlockerRectangle.Y = 0
        BlockerRectangle.Width = pctSofticeOutput.Width
        BlockerRectangle.Height = (pctSofticeOutput.Height - RectangleCupFront.Height) * (Blocker / 100)
        g.FillRectangle(Brushes.White, BlockerRectangle)


    End Sub


    Private Function ReturnFittingRectangle(ByVal width As Integer, ByVal height As Integer, ByVal img As Image, Optional ByVal SizeMultiplier As Single = 1) As Rectangle

        Dim ImageWidthHeightRatio As Single
        Dim BoxToImageRatio As Single
        Dim RectangleWidth, RectangleHeight As Single
        Dim RectangleOrigoWidth, RectangleOrigoHeight As Integer

        ImageWidthHeightRatio = img.Width / img.Height

        If ImageWidthHeightRatio > 1 Then
            ' If image's width > image's height.
            BoxToImageRatio = width / img.Width
            RectangleWidth = img.Width * BoxToImageRatio
            RectangleHeight = RectangleWidth / ImageWidthHeightRatio
        ElseIf ImageWidthHeightRatio < 1 Then
            ' Else, if image's height > image's width.
            BoxToImageRatio = height / img.Height
            RectangleHeight = img.Height * BoxToImageRatio
            RectangleWidth = RectangleHeight * ImageWidthHeightRatio
        Else
            ' If the image is square.
            RectangleHeight = img.Height * BoxToImageRatio
            RectangleWidth = img.Width * BoxToImageRatio
        End If

        RectangleWidth = Convert.ToInt32(RectangleWidth * SizeMultiplier / 2)
        RectangleHeight = Convert.ToInt32(RectangleHeight * SizeMultiplier / 2)

        ' Centering the rectangle horizontally inside the given box.
        If width = RectangleWidth Then
            RectangleOrigoWidth = 0
        Else
            Dim delta As Integer = Convert.ToInt32((width - RectangleWidth) / 2)
            RectangleOrigoWidth += delta
        End If

        ' Moving it to the very bottom.
        RectangleOrigoHeight = height - RectangleHeight
        'If height = RectangleHeight Then
        '    RectangleOrigoHeight = 0
        'Else
        '    Dim delta As Integer = Convert.ToInt32((height - RectangleHeight) / 2)
        '    RectangleOrigoHeight += delta
        'End If

        Return New Rectangle(RectangleOrigoWidth, RectangleOrigoHeight, RectangleWidth, RectangleHeight)

    End Function


    Private Sub pctSofticeOutput_Click(sender As System.Object, e As System.EventArgs) Handles pctSofticeOutput.Click

        ResetCosts()
        UpdateCosts()
        UpdateInterface()
        InitiateSelectionMode()
        'MsgBox("Is invisible nao!")
        pctSofticeOutput.Visible = False

        HasHadSoftServe = True

    End Sub


    Private Sub tmrSofticeAnimation_Tick(sender As System.Object, e As System.EventArgs) Handles tmrSofticeAnimation.Tick

        If Blocker = 0 Then
            tmrSofticeAnimation.Stop()
            Blocker = 100
            Return
        End If

        Blocker -= 10


        AnimateSoftice(Blocker)

    End Sub

End Class
