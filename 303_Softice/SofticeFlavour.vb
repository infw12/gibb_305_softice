﻿Public Class SofticeFlavour

    Friend Name As String
    Friend BasePrice As Single

    Friend Picture As Bitmap

    Friend Sub New(ByVal Name As String, ByVal BasePrice As Single, ByVal Picture As Bitmap)

        Me.Name = Name
        Me.BasePrice = BasePrice
        Me.Picture = Picture

    End Sub

    Friend Function PrettyPrint() As String
        Return String.Format("{0} ({1})", Name, frmMain.FormatCurrency(Me.BasePrice))
    End Function

End Class
